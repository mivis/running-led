include settings.mk

ifeq ($(TARGET_MCU),STM32L051xx)
	STARTUP = startup/startup_stm32l051xx_aranet.s
	LINKER_SCRIPT = ./linker/linker_stm32l051xx.ld
	JLINK_MCU = STM32L051K8
	RAM=8
	FLASH_SIZE=64
else 
ifeq ($(TARGET_MCU),STM32L011xx)
	STARTUP = startup/startup_stm32l011xx_aranet.s
	LINKER_SCRIPT = ./linker/linker_stm32l051xx.ld
	JLINK_MCU = STM32L051K8
	RAM=2
	FLASH_SIZE=16
endif
endif

# List all C defines here
VERSION =0
DEFS += -DSTM32L0 -D$(TARGET_MCU) -DFW_VERSION=$(VERSION) -DSTM32L051xx

# Debug level
#DEBUG = -g3 #-Wa,-adhlns="$@.lst"
DEBUG = -g3

# Define optimisation level here
OPT = -Os

# MCU type
MCU  = cortex-m0plus

# JLink config
JLINKARGS = -if swd -device $(JLINK_MCU) -speed 1000 -autoconnect 1

# Tools
PREFIX = arm-none-eabi-
CC   = $(PREFIX)gcc
CXX  = $(PREFIX)g++
GDB  = $(PREFIX)gdb
CP   = $(PREFIX)objcopy
AS   = $(PREFIX)gcc -x assembler-with-cpp
SIZE = $(PREFIX)size
HEX  = $(CP) -O ihex
BIN  = $(CP) -O binary -S

#Path to Texane's stlink tools
STLINK = /home/lskalski/Programs/stlink

# List of source files
SRC  = src/main.cpp
SRC += src/interrupts.cpp
SRC += src/system_syscalls.c
SRC += src/system_stm32l0xx.c

SRC += src/interfaces/led.cpp
SRC += src/interfaces/gpio.cpp

SRC += src/peripherals/lptimer.cpp
SRC += src/peripherals/rcc.cpp

SRC += src/utilities/utils.cpp
SRC += src/utilities/general.cpp

# List assembly startup source file here
#STARTUP = startup/startup_stm32l051xx.s
 
# List all include directories here
INCDIRS = ./src ./inc ./inc/CMSIS/core ./inc/CMSIS/device

BINDIR = bin
              
# List the user directory to look for the libraries here
LIBDIRS += 
 
# List all user libraries here
LIBS = m

# Define linker script file here
#LINKER_SCRIPT = ./linker/linker_stm32l051xx.ld

# Dirs
OBJS  = $(addprefix objs/, $(STARTUP:.s=.o) $(addsuffix .o, $(basename $(SRC))))
DEPS  = $(patsubst objs/%, deps/%, $(OBJS:.o=.d))
INCDIR  = $(patsubst %,-I%, $(INCDIRS))
LIBDIR  = $(patsubst %,-L%, $(LIBDIRS))
LIB     = $(patsubst %,-l%, $(LIBS))

# Flags
COMMONFLAGS = -mcpu=$(MCU) -mthumb -mfloat-abi=soft -fsingle-precision-constant --specs=nano.specs -Wdouble-promotion
ASFLAGS = $(COMMONFLAGS) $(DEBUG)
CFLAGS  = $(COMMONFLAGS) $(OPT) $(DEFS) $(DEBUG) -Wall -Wextra -ffunction-sections -fdata-sections -finline-small-functions -findirect-inlining
CPFLAGS = $(COMMONFLAGS) $(OPT) $(DEFS) $(DEBUG) -Wall -Wextra -ffunction-sections -fdata-sections -finline-small-functions -findirect-inlining -fno-exceptions -fno-rtti 
LDFLAGS = $(COMMONFLAGS) -T$(LINKER_SCRIPT) -Wl,-Map=$(BINDIR)/$(PROJECT).map -Wl,--gc-sections $(LIBDIR) $(LIB)
 
# Helper for colorizing output
define colorecho
      @printf "$1: "
      @if tty -s; then tput setaf $3; fi
      @echo $2
      @if tty -s; then tput sgr0; fi
endef

#
# Makefile Rules
#
 
UNAME_S = $(shell uname -s)

all: $(OBJS) $(PROJECT).elf  $(PROJECT).hex $(PROJECT).bin

analyze_elf_contents: $(PROJECT).elf
	@arm-none-eabi-nm $(PROJECT).elf -S --size-sort -t d -C
	@./scripts/calcsize.py $(PROJECT).elf $(FLASH_SIZE) $(RAM)
	
objs/%.o: %.c
	$(call colorecho, "Compiling", "$<", 6)
	@mkdir -p objs/$(dir $<)
	@mkdir -p deps/$(dir $<)
	@$(CC) -c $(CFLAGS) -I . $(INCDIR)  -MMD -MP -MF deps/$(basename $<).d $< -o $@

objs/%.o: %.cpp
	$(call colorecho, "Compiling", "$<", 6)
	@mkdir -p objs/$(dir $<)
	@mkdir -p deps/$(dir $<)
	@$(CC) -c $(CPFLAGS) -I . $(INCDIR) -MMD -MP -MF deps/$(basename $<).d $< -o $@

objs/%.o: %.s
	$(call colorecho, "Compiling", "$<", 6)
	@mkdir -p objs/$(dir $<)
	@$(AS) -c $(ASFLAGS) $< -o $@

-include $(DEPS)

%.elf: $(OBJS)
	@mkdir -p $(BINDIR)
	$(call colorecho, "Linking", "$@", 6)
	@$(CC) $(OBJS) $(LDFLAGS) -o $(BINDIR)/$@
	@$(TRGT)size $(BINDIR)/$(PROJECT).elf
	@./scripts/calcsize.py $(BINDIR)/$(PROJECT).elf $(FLASH_SIZE) $(RAM)

%.hex: $(BINDIR)/%.elf
	$(call colorecho, "Converting", "$@", 6)
	@$(HEX) $< $(BINDIR)/$@

%.bin: $(BINDIR)/%.elf
	$(call colorecho, "Converting", "$@", 6)
	@$(BIN) $< $(BINDIR)/$@

flash: $(BINDIR)/$(PROJECT).bin
	openocd -f scripts/openocd_stm32lx_jlink.cfg -c "init" -c "reset halt" -c "program $(BINDIR)/$(PROJECT).elf" -c "exit"

jflash: $(BINDIR)/$(PROJECT).bin $(BINDIR)/$(PROJECT).hex
	@echo "Flashing device."
	@cp scripts/flash.jlink scripts/~flash.jlink
	sed -i -e 's|{{PROJECT}}|$(BINDIR)/$(PROJECT).hex|g' scripts/~flash.jlink
	@JLinkExe $(JLINKARGS) -commanderscript scripts/~flash.jlink
	@rm scripts/~flash.jlink

lockdevice:
	@echo "Locking device."
	@JLinkExe $(JLINKARGS) -commanderscript scripts/stm32l0_lock.jlink

unlockdevice:
	@echo "Unlocking/erasing device."
	@JLinkExe $(JLINKARGS) -commanderscript scripts/stm32l0_unlock.jlink


erase:
	openocd -f scripts/openocd_stm32lx_jlink.cfg -c "erase" -c "exit"

debug: $(PROJECT).elf runjlinkserver
	arm-none-eabi-gdb -iex "file $(BINDIR)/$(PROJECT).elf" -iex="target remote localhost:2331" -ex "monitor reset"

runjlinkserver: $(PROJECT).elf
ifeq ($(UNAME_S), Darwin)
	osascript -e "tell app \"Terminal\" to do script \"cd $$(pwd); make jlinkserver && exit\""
	osascript -e "tell front window of application \"Terminal\" to set selected tab to tab 1"
else 
	echo "Running jlinkGDBserver like this is not supported in this script for this OS yet! Just add an entry that can run it in a seperate terminal."  
endif

jlinkserver: $(PROJECT).elf 
	JLinkGDBServer -if swd -device STM32L051K8 -speed 1000 -singlerun -timeout 0

clean:
	-rm -rf deps/ -r
	-rm -rf objs/ -r
	-rm -rf $(OBJS)
	-rm -rf $(BINDIR)/$(PROJECT).*
	-rm -rf $(addsuffix .lst, $(OBJS))
	-rm -rf $(ASRC:.s=.lst)
	-rm -rf $(STARTUP:.s=.lst)
	#-rmdir $(BINDIR)

print-%  : ; @echo $* = $($*)
