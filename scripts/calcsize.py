#!/usr/bin/env python3

import os
import sys
import re

romKb = 16
ramKb = 2

if len(sys.argv) == 2:
    progfile = sys.argv[1]
elif len(sys.argv) == 3:
    progfile = sys.argv[1]
    romKb = int(sys.argv[2])
elif len(sys.argv) == 4:
    progfile = sys.argv[1]
    romKb = int(sys.argv[2])
    ramKb = int(sys.argv[3])
else:
    progfile = 'bin/MyProject.elf'

ramSize   = ramKb * 1024
flashSize = romKb * 1024

sizepipe = os.popen(f'size {progfile} | tail -1')
sizedata = sizepipe.read()
sizepipe.close()

text, data, bss, dec = [ int(size) for size in re.findall('\S+', sizedata)[0:4] ]


print('')
print(f'Flash is {text + data: >6d}B/{flashSize: >6d}B\t{(text + data)/flashSize * 100: >5.1f}%\toccupied')
print(f'RAM is   {data + bss: >6d}B/{ramSize: >6d}B\t{(data + bss)/ramSize * 100: >5.1f}%\toccupied')
print('')
