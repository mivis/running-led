#include "led.hpp"

#include "stm32l0xx.h"

#include "board_config.hpp"
#include "utilities/utils.hpp"
#include "interfaces/gpio.hpp"


void cLED::init(GPIO_TypeDef* port, uint8_t pin) {
	if(!initialised && enabled) {
		cGPIO::pinConf(port, pin, cGPIO::DIGITAL_OUTPUT);
		initialised = true;
	}
}

void cLED::deinit(GPIO_TypeDef* port, uint8_t pin) {
	if(initialised) {
		cGPIO::pinConf(port, pin, cGPIO::ANALOG);
		initialised = false;
	}
}

void cLED::set(bool on) {
  if(on){
	init(cport, cpin);
    cGPIO::pinSet(cport, cpin, true);
  } else {
	cGPIO::pinSet(cport, cpin, false);
	deinit(cport, cpin);
  }
}

void cLED::toggle(GPIO_TypeDef* port, uint8_t pin) {
	init(port, pin);
	cGPIO::pinToggle(port, pin);
}
