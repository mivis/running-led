#ifndef __LED_HPP
#define __LED_HPP

#include <stm32l0xx.h>
#include "board_config.hpp"

class cLED {
public:
  GPIO_TypeDef* cport; 
  uint8_t cpin;
	cLED(GPIO_TypeDef* port, uint8_t pin){
		initialised = false;
		enabled = true;
    cport = port;
    cpin = pin;
	}

 public:
  bool initialised; // various LED's are possible
  bool enabled; // various LED's are possible
  void init(GPIO_TypeDef* port, uint8_t pin);
  void deinit(GPIO_TypeDef* port, uint8_t pin);
  void enable(bool active, GPIO_TypeDef* port, uint8_t pin);
  void set(bool on);
  void toggle(GPIO_TypeDef* port=LEDPORT, uint8_t pin=LEDPIN);
};



#endif  // __LED_HPP
