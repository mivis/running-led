#include "interfaces/uart.hpp"

#include "stm32l0xx.h"

#include "board_config.hpp"
#include "interfaces/gpio.hpp"
#include "utilities/soft_ms_timer.hpp"

USART_TypeDef* USART_TARGET = USART1;
IRQn_Type USART_IRQ = USART1_IRQn;
uint32_t USART_CLOCK = RCC_APB2ENR_USART1EN;
bool cUART::usingOnewire = false;

void cUART::init(uint8_t usartNumber, int32_t baudRate, bool oneWire, bool swapPins, bool invertTTL) {
  usingOnewire = oneWire;
  gpioInit(usartNumber);
  if(USART_TARGET == USART2) {
    RCC->APB1ENR |= USART_CLOCK;
  } else {
    RCC->APB2ENR |= USART_CLOCK;
  }
  setBaud(baudRate);
  if(oneWire) {
    USART_TARGET->CR2 &= ~(USART_CR2_LINEN | USART_CR2_CLKEN);
    USART_TARGET->CR3 &= ~(USART_CR3_IREN | USART_CR3_SCEN);
    USART_TARGET->CR3 |= USART_CR3_HDSEL;
  }
  if(swapPins) {
    USART_TARGET->CR2 |= USART_CR2_SWAP;
  }
  if(invertTTL)
    USART1->CR2 |= USART_CR2_RXINV;
  USART_TARGET->CR1 |= (USART_CR1_TE | USART_CR1_RE | USART_CR1_UE);
}

void cUART::deinit() {
  USART_TARGET->CR1 &= ~USART_CR1_UE;
  if(USART_TARGET == USART2) {
    RCC->APB1ENR &= ~USART_CLOCK;
  } else {
    RCC->APB2ENR &= ~USART_CLOCK;
  }
}

void cUART::setBaud(int32_t baud) {
  if(!baud) {
    baud = 9600;
  }
  USART_TARGET->BRR = MSI_freq / baud;
}

void cUART::gpioInit(uint8_t USART1Num) {
  uint8_t TXpin;
  uint8_t RXpin;

  switch(USART1Num) {
    default:
    case UART_1A:
      TXpin = 9;
      RXpin = 10;
      USART_TARGET = USART1;
      USART_IRQ = USART1_IRQn;
      USART_CLOCK = RCC_APB2ENR_USART1EN;
      break;
    case UART_2A:
      TXpin = 2;
      RXpin = 3;
      USART_TARGET = USART2;
      USART_IRQ = USART2_IRQn;
      USART_CLOCK = RCC_APB1ENR_USART2EN;
      break;
    case UART_2B:
      TXpin = 14;
      RXpin = 15;
      USART_TARGET = USART2;
      USART_IRQ = USART2_IRQn;
      USART_CLOCK = RCC_APB1ENR_USART2EN;
      break;
  }

  if(usingOnewire) {
    cGPIO::pinInit(GPIOA, TXpin, cGPIO::ALTERNATE, cGPIO::OTYPE_OD, cGPIO::OSPEED_HI, cGPIO::PUPD_NO, cGPIO::AF4);
  } else {
    cGPIO::pinInit(GPIOA, TXpin, cGPIO::ALTERNATE, cGPIO::OTYPE_PP, cGPIO::OSPEED_HI, cGPIO::PUPD_NO, cGPIO::AF4);
    cGPIO::pinInit(GPIOA, RXpin, cGPIO::ALTERNATE, cGPIO::OTYPE_PP, cGPIO::OSPEED_HI, cGPIO::PUPD_NO, cGPIO::AF4);
  }
}

bool cUART::rx(char* c, const int32_t timeoutms) {
  cSoftMsTimer timer;
  while(!(USART_TARGET->ISR & USART_ISR_RXNE) && (timer.elapsed() < timeoutms)) {};

  if(timer.elapsed() >= timeoutms) {
    return false;
  } else {
    *c = USART_TARGET->RDR;
  }
  return true;
}

uint32_t cUART::rx(char* byte, const int32_t length, const int32_t initialTimeout, const uint32_t interbyteTimeout) {
  for(int32_t i = 0; i < length; i++) {
    if(!rx(byte + i, i ? interbyteTimeout : initialTimeout)) {
      return i;
    }
  }
  return length;
}

bool cUART::tx(char byte, const int32_t timeoutms) {
  cSoftMsTimer timer;
  while(!(USART_TARGET->ISR & USART_ISR_TXE) && (timer.elapsed() < timeoutms)) {};

  if(timer.elapsed() >= timeoutms) {
    return false;
  } else {
    if(usingOnewire) {
      USART_TARGET->CR1 &= ~USART_CR1_RE;
      USART_TARGET->TDR = byte;
      while(txBusy()) {};
      USART_TARGET->CR1 |= USART_CR1_RE;
    } else {
      USART_TARGET->TDR = byte;
    }
  }
  return true;
}

uint32_t cUART::tx(char* byte, const int32_t length, const int32_t timeoutms) {
  for(int32_t i = 0; i < length; i++) {
    if(!tx(byte[i], timeoutms)) {
      return i;
    }
  }
  return length;
}

void cUART::rxISREnable(bool state, bool wakeInStopMode) {
  bool enabled = USART_TARGET->CR1 & USART_CR1_UE;
  USART_TARGET->CR1 &= ~USART_CR1_UE;

  if(wakeInStopMode) {
    // Configure UART to run from HSI16 / 4
    // This increases stop mode power consumption, should only be used for in-house dev.
    USART_TARGET->CR1 |= USART_CR1_UESM;
    if(USART_TARGET == USART2) {
      RCC->APB1SMENR |= RCC_APB1SMENR_USART2SMEN;
      RCC->CCIPR |= RCC_CCIPR_USART2SEL_1;
    } else {
      RCC->APB2SMENR |= RCC_APB2SMENR_USART1SMEN;
      RCC->CCIPR |= RCC_CCIPR_USART1SEL_1;
    }
    RCC->CR |= RCC_CR_HSIDIVEN;
    RCC->CR |= RCC_CR_HSIKERON;
    RCC->CR |= RCC_CR_HSION;
  }

  if(state) {
    USART_TARGET->CR1 |= USART_CR1_RXNEIE;
    USART_TARGET->RQR |= USART_RQR_RXFRQ;
    NVIC_EnableIRQ(USART_IRQ);
  } else {
    USART_TARGET->CR1 &= ~USART_CR1_RXNEIE;
    NVIC_DisableIRQ(USART_IRQ);
  }

  if(enabled) {
    USART_TARGET->CR1 |= USART_CR1_UE;
  }
}

bool cUART::txBusy() {
  return !(USART_TARGET->ISR & USART_ISR_TC);
}

// Function call required by printf module,
// only if printf is used explicity.
extern "C" void _putchar(char c) __attribute__((weak));
extern "C" void _putchar(char c) {
  cUART::tx(c);
}
