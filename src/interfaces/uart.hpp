#ifndef UART_HPP
#define UART_HPP

#include <cstdint>

class cUART {
 public:
  enum Uart_Types {
    UART_1A,  // STM32L051 Arranet development board. Connected to PA9, PA10
    UART_2A,  // STM32L053 Nucleo, connected to STlink VCOM PA2, PA3
    UART_2B,  // STM32L051 Aranet sensor connected to PA14 and PA15
  };

  // UART is initialized (by default) with 1 stop bit, no parity and 115200 baud.
  static void init(uint8_t usartNumber, int32_t baudRate = 115200, bool oneWire = false, bool swapPins = false, bool invertTTL = false);
  static void deinit();
  static void setBaud(int32_t baud);
  static bool tx(char byte, const int32_t timeoutms = 1000);
  static uint32_t tx(char* byte, const int32_t length, const int32_t timeoutms);
  static bool rx(char* byte, const int32_t timeoutms = 1000);
  static uint32_t rx(char* byte, const int32_t length, const int32_t initialTimeout, const uint32_t interbyteTimeout = 10);
  static bool txBusy();
  static void rxISREnable(bool state, bool wakeInStopMode = false);

 private:
  static void gpioInit(uint8_t peripheral);
  static bool usingOnewire;
};
#endif
