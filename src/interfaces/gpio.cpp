#include "gpio.hpp"

#include <stm32l0xx.h>

#include "utilities/utils.hpp"

void cGPIO::pinInit(GPIO_TypeDef* port, uint8_t pin, Gpio_Init_Mode mode, Gpio_Init_Type type, Gpio_Init_Speed speed, Gpio_Init_PuPd pupd,
                    Gpio_Init_Afr afr) {
  if(port == GPIOA) {
    SET_BIT(RCC->IOPENR, RCC_IOPENR_GPIOAEN);
  } else if(port == GPIOB) {
    SET_BIT(RCC->IOPENR, RCC_IOPENR_GPIOBEN);
  } else if(port == GPIOC) {
    SET_BIT(RCC->IOPENR, RCC_IOPENR_GPIOCEN);
  } else {
    // Do nothing
  }

  //------ MODE Register -------------------------------------------
  switch(mode) {
    case DIGITAL_INPUT: MODIFY_REG(port->MODER, 0x3 << (pin * 2), 0x0 << (pin * 2)); break;

    case DIGITAL_OUTPUT: MODIFY_REG(port->MODER, 0x3 << (pin * 2), 0x1 << (pin * 2)); break;

    case ALTERNATE:  // Alternate function mode
      MODIFY_REG(port->MODER, 0x3 << (pin * 2), 0x2 << (pin * 2));
      break;

    default:
    case ANALOG:  // Analog mode (reset state)
      MODIFY_REG(port->MODER, 0x3 << (pin * 2), 0x3 << (pin * 2));
      break;
  }


  //------ OTYPE Register -------------------------------------------
  switch(type) {
    default:
    case OTYPE_PP:  // Push-pull
      MODIFY_REG(port->OTYPER, (0x1 << pin), 0x0 << pin);
      break;

    case OTYPE_OD:  // Open drain
      MODIFY_REG(port->OTYPER, (0x1 << pin), 0x1 << pin);
      break;
  }

  //------ OSPEED Register -------------------------------------------
  switch(speed) {
    default:
    case OSPEED_LOW:
      MODIFY_REG(port->OSPEEDR, 0x3 << (pin * 2), 0x0 << (pin * 2));  // Low speed (MHz value dependent from supply voltage and used device)
      break;

    case OSPEED_MED:
      MODIFY_REG(port->OSPEEDR, 0x3 << (pin * 2), 0x1 << (pin * 2));  // Medium speed (2MHz for L011 at Vdd>2.7V)
      break;

    case OSPEED_HI:
      MODIFY_REG(port->OSPEEDR, 0x3 << (pin * 2), 0x2 << (pin * 2));  // High speed 10MHz
      break;

    case OSPEED_VHI:
      MODIFY_REG(port->OSPEEDR, 0x3 << (pin * 2), 0x3 << (pin * 2));  // Very high speed 40MHz
      break;
  }

  //------ PUPD Register -------------------------------------------
  switch(pupd) {
    default:
    case PUPD_NO:
      MODIFY_REG(port->PUPDR, 0x3 << (pin * 2), 0x0 << (pin * 2));  // No pull-up, pull-down
      break;

    case PUPD_PU:
      MODIFY_REG(port->PUPDR, 0x3 << (pin * 2), 0x1 << (pin * 2));  // Pull-up
      break;

    case PUPD_PD:
      MODIFY_REG(port->PUPDR, 0x3 << (pin * 2), 0x2 << (pin * 2));  // Pull-down
      break;
  }

  //------ AFR Register -------------------------------------------
  // AF8 to AF15 - reserved
  switch(afr) {
    default:
    case AF0:
      MODIFY_REG(port->AFR[pin / 8], 0xF << ((pin % 8) * 4), 0x0 << ((pin % 8) * 4));  // AF0
      break;

    case AF1:
      MODIFY_REG(port->AFR[pin / 8], 0xF << ((pin % 8) * 4), 0x1 << ((pin % 8) * 4));  // AF1
      break;

    case AF2:
      MODIFY_REG(port->AFR[pin / 8], 0xF << ((pin % 8) * 4), 0x2 << ((pin % 8) * 4));  // AF2
      break;

    case AF3:
      MODIFY_REG(port->AFR[pin / 8], 0xF << ((pin % 8) * 4), 0x3 << ((pin % 8) * 4));  // AF3
      break;

    case AF4:
      MODIFY_REG(port->AFR[pin / 8], 0xF << ((pin % 8) * 4), 0x4 << ((pin % 8) * 4));  // AF4
      break;

    case AF5:
      MODIFY_REG(port->AFR[pin / 8], 0xF << ((pin % 8) * 4), 0x5 << ((pin % 8) * 4));  // AF5
      break;

    case AF6:
      MODIFY_REG(port->AFR[pin / 8], 0xF << ((pin % 8) * 4), 0x6 << ((pin % 8) * 4));  // AF6
      break;

    case AF7:
      MODIFY_REG(port->AFR[pin / 8], 0xF << ((pin % 8) * 4), 0x7 << ((pin % 8) * 4));  // AF7
      break;
  }

  return;
}

void cGPIO::pinConf(GPIO_TypeDef* port, uint8_t pin, Gpio_Init_Mode mode) {
  if(port == GPIOA) {
    SET_BIT(RCC->IOPENR, RCC_IOPENR_GPIOAEN);
  } else if(port == GPIOB) {
    SET_BIT(RCC->IOPENR, RCC_IOPENR_GPIOBEN);
  } else if(port == GPIOC) {
    SET_BIT(RCC->IOPENR, RCC_IOPENR_GPIOCEN);
  } else {
    // Do nothing
  }

  //------ MODE Register -------------------------------------------
  switch(mode) {
    case DIGITAL_INPUT: MODIFY_REG(port->MODER, 0x3 << (pin * 2), 0x0 << (pin * 2)); break;

    case DIGITAL_OUTPUT: MODIFY_REG(port->MODER, 0x3 << (pin * 2), 0x1 << (pin * 2)); break;

    case ALTERNATE:  // Alternate function mode
      MODIFY_REG(port->MODER, 0x3 << (pin * 2), 0x2 << (pin * 2));
      break;

    default:
    case ANALOG:  // Analog mode (reset state)
      MODIFY_REG(port->MODER, 0x3 << (pin * 2), 0x3 << (pin * 2));
      break;
  }
}

void cGPIO::pinConf(GPIO_TypeDef* port, uint8_t pin, Gpio_Init_Type type) {
  if(port == GPIOA) {
    SET_BIT(RCC->IOPENR, RCC_IOPENR_GPIOAEN);
  } else if(port == GPIOB) {
    SET_BIT(RCC->IOPENR, RCC_IOPENR_GPIOBEN);
  } else if(port == GPIOC) {
    SET_BIT(RCC->IOPENR, RCC_IOPENR_GPIOCEN);
  } else {
    // Do nothing
  }

  //------ OTYPE Register -------------------------------------------
  switch(type) {
    default:
    case OTYPE_PP:  // Push-pull
      MODIFY_REG(port->OTYPER, (0x1 << pin), 0x0 << pin);
      break;

    case OTYPE_OD:  // Open drain
      MODIFY_REG(port->OTYPER, (0x1 << pin), 0x1 << pin);
      break;
  }
}
void cGPIO::pinConf(GPIO_TypeDef* port, uint8_t pin, Gpio_Init_Speed speed) {
  if(port == GPIOA) {
    SET_BIT(RCC->IOPENR, RCC_IOPENR_GPIOAEN);
  } else if(port == GPIOB) {
    SET_BIT(RCC->IOPENR, RCC_IOPENR_GPIOBEN);
  } else if(port == GPIOC) {
    SET_BIT(RCC->IOPENR, RCC_IOPENR_GPIOCEN);
  } else {
    // Do nothing
  }

  //------ OSPEED Register -------------------------------------------
  switch(speed) {
    default:
    case OSPEED_LOW:
      MODIFY_REG(port->OSPEEDR, 0x3 << (pin * 2), 0x0 << (pin * 2));  // Low speed (MHz value dependent from supply voltage and used device)
      break;

    case OSPEED_MED:
      MODIFY_REG(port->OSPEEDR, 0x3 << (pin * 2), 0x1 << (pin * 2));  // Medium speed (2MHz for L011 at Vdd>2.7V)
      break;

    case OSPEED_HI:
      MODIFY_REG(port->OSPEEDR, 0x3 << (pin * 2), 0x2 << (pin * 2));  // High speed 10MHz
      break;

    case OSPEED_VHI:
      MODIFY_REG(port->OSPEEDR, 0x3 << (pin * 2), 0x3 << (pin * 2));  // Very high speed 40MHz
      break;
  }
}
void cGPIO::pinConf(GPIO_TypeDef* port, uint8_t pin, Gpio_Init_PuPd pupd) {
  if(port == GPIOA) {
    SET_BIT(RCC->IOPENR, RCC_IOPENR_GPIOAEN);
  } else if(port == GPIOB) {
    SET_BIT(RCC->IOPENR, RCC_IOPENR_GPIOBEN);
  } else if(port == GPIOC) {
    SET_BIT(RCC->IOPENR, RCC_IOPENR_GPIOCEN);
  } else {
    // Do nothing
  }

  //------ PUPD Register -------------------------------------------
  switch(pupd) {
    default:
    case PUPD_NO:
      MODIFY_REG(port->PUPDR, 0x3 << (pin * 2), 0x0 << (pin * 2));  // No pull-up, pull-down
      break;

    case PUPD_PU:
      MODIFY_REG(port->PUPDR, 0x3 << (pin * 2), 0x1 << (pin * 2));  // Pull-up
      break;

    case PUPD_PD:
      MODIFY_REG(port->PUPDR, 0x3 << (pin * 2), 0x2 << (pin * 2));  // Pull-down
      break;
  }
}
void cGPIO::pinConf(GPIO_TypeDef* port, uint8_t pin, Gpio_Init_Afr afr) {
  if(port == GPIOA) {
    SET_BIT(RCC->IOPENR, RCC_IOPENR_GPIOAEN);
  } else if(port == GPIOB) {
    SET_BIT(RCC->IOPENR, RCC_IOPENR_GPIOBEN);
  } else if(port == GPIOC) {
    SET_BIT(RCC->IOPENR, RCC_IOPENR_GPIOCEN);
  } else {
    // Do nothing
  }

  //------ AFR Register -------------------------------------------
  // AF8 to AF15 - reserved
  switch(afr) {
    default:
    case AF0:
      MODIFY_REG(port->AFR[pin / 8], 0xF << ((pin % 8) * 4), 0x0 << ((pin % 8) * 4));  // AF0
      break;

    case AF1:
      MODIFY_REG(port->AFR[pin / 8], 0xF << ((pin % 8) * 4), 0x1 << ((pin % 8) * 4));  // AF1
      break;

    case AF2:
      MODIFY_REG(port->AFR[pin / 8], 0xF << ((pin % 8) * 4), 0x2 << ((pin % 8) * 4));  // AF2
      break;

    case AF3:
      MODIFY_REG(port->AFR[pin / 8], 0xF << ((pin % 8) * 4), 0x3 << ((pin % 8) * 4));  // AF3
      break;

    case AF4:
      MODIFY_REG(port->AFR[pin / 8], 0xF << ((pin % 8) * 4), 0x4 << ((pin % 8) * 4));  // AF4
      break;

    case AF5:
      MODIFY_REG(port->AFR[pin / 8], 0xF << ((pin % 8) * 4), 0x5 << ((pin % 8) * 4));  // AF5
      break;

    case AF6:
      MODIFY_REG(port->AFR[pin / 8], 0xF << ((pin % 8) * 4), 0x6 << ((pin % 8) * 4));  // AF6
      break;

    case AF7:
      MODIFY_REG(port->AFR[pin / 8], 0xF << ((pin % 8) * 4), 0x7 << ((pin % 8) * 4));  // AF7
      break;
  }
}

void cGPIO::pinSet(GPIO_TypeDef* port, uint8_t pin, bool state) {
  if(state) {
    SET_BIT(port->BSRR, 0x1 << pin);
  } else {
    SET_BIT(port->BRR, 0x1 << pin);
  }
  return;
}

uint8_t cGPIO::pinRead(GPIO_TypeDef* port, uint8_t pin) {
  return READ_BIT(port->IDR, (0x1 << pin));
}

void cGPIO::pinToggle(GPIO_TypeDef* port, uint8_t pin) {
	TOGGLE_BIT(port->ODR, 0x1 << pin);
}
