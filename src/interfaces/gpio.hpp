#ifndef SRC_INTERFACES_GPIO_HPP_
#define SRC_INTERFACES_GPIO_HPP_
#include <stm32l0xx.h>
#include <cstdint>

class cGPIO {
 public:
  enum Gpio_Init_Mode { DIGITAL_INPUT, DIGITAL_OUTPUT, ALTERNATE, ANALOG };

  enum Gpio_Init_Type {
    OTYPE_PP,  // Push-pull
    OTYPE_OD,  // Open drain
  };

  enum Gpio_Init_Speed {
    OSPEED_LOW,  // Low speed (MHz value dependent from supply voltage and used device)
    OSPEED_MED,  // Medium speed (2MHz for L011 at Vdd>2.7V)
    OSPEED_HI,   // High speed
    OSPEED_VHI,  // Very high speed
  };

  enum Gpio_Init_PuPd {
    PUPD_NO,  // No pull-up, pull-down
    PUPD_PU,  // Pull-up
    PUPD_PD,  // Pull-down
  };

  enum Gpio_Init_Afr {
    AF0,  // AF0
    AF1,  // AF1
    AF2,  // AF2
    AF3,  // AF3
    AF4,  // AF4
    AF5,  // AF5
    AF6,  // AF6
    AF7,  // AF7
  };


  static void pinInit(GPIO_TypeDef* port, uint8_t pin, Gpio_Init_Mode mode = ANALOG, Gpio_Init_Type type = OTYPE_PP,
                      Gpio_Init_Speed speed = OSPEED_LOW, Gpio_Init_PuPd pupd = PUPD_NO, Gpio_Init_Afr afr = AF0);


  static void pinConf(GPIO_TypeDef* port, uint8_t pin, Gpio_Init_Mode mode = ANALOG);
  static void pinConf(GPIO_TypeDef* port, uint8_t pin, Gpio_Init_Type type = OTYPE_PP);
  static void pinConf(GPIO_TypeDef* port, uint8_t pin, Gpio_Init_Speed speed = OSPEED_LOW);
  static void pinConf(GPIO_TypeDef* port, uint8_t pin, Gpio_Init_PuPd pupd = PUPD_NO);
  static void pinConf(GPIO_TypeDef* port, uint8_t pin, Gpio_Init_Afr afr = AF0);

  static void pinSet(GPIO_TypeDef* port, uint8_t pin, bool state);
  static uint8_t pinRead(GPIO_TypeDef* port, uint8_t pin);
  static void pinToggle(GPIO_TypeDef* port, uint8_t pin);

 private:
 protected:
};

#endif /* SRC_INTERFACES_GPIO_HPP_ */
