#ifndef __BOARD_CONFIG_HPP
#define __BOARD_CONFIG_HPP

// MSI Frequency
// #define MSI_freq  ((uint32_t)1048000)
// #define MSI_freq  ((uint32_t)2097000)
#define MSI_freq  ((uint32_t)4194000)

// Debug
#define DEBUG

// Programming connector
#define SW_POWERBIT RCC_IOPENR_GPIOAEN
#define SWD_PORT    GPIOA
#define SWD_PIN     13
#define SWC_PORT    GPIOA
#define SWC_PIN     14

// Button
#define BUTTON_GPIO_POWERBIT RCC_IOPENR_GPIOBEN
#define BUTTON_PORT          GPIOB
#define BUTTON_PIN           4

// default LED Pin
#define LEDPOWERBIT RCC_IOPENR_GPIOAEN
#define LEDPORT     GPIOA
#define LEDPIN     15

#endif  // __BOARD_CONFIG_HPP
