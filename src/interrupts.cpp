#include "stm32l0xx.h"

#include "peripherals/lptimer.hpp"
#include "utilities/utils.hpp"

extern "C" void SysTick_Handler(void) __attribute__((weak));
extern "C" void SysTick_Handler(void) {
  cUtils::tick();
}

extern "C" void WWDG_IRQHandler(void) __attribute__((weak));
extern "C" void WWDG_IRQHandler(void) {
  while(1)
    __NOP();
}

extern "C" void NMI_Handler(void) __attribute__((weak));
extern "C" void NMI_Handler(void) {
  while(1)
    __NOP();
}

extern "C" void HardFault_Handler(void) __attribute__((weak));
extern "C" void HardFault_Handler(void) {
  // In the rare case we get here, a reset is the best option.
  NVIC_SystemReset();
  while(1)
    ;  // Wait here incase reset failed somehow.
}

extern "C" void SVC_Handler(void) __attribute__((weak));
extern "C" void SVC_Handler(void) {
  while(1)
    __NOP();
}

extern "C" void PendSV_Handler(void) __attribute__((weak));
extern "C" void PendSV_Handler(void) {
  while(1)
    __NOP();
}

extern "C" void DebugMon_Handler(void) __attribute__((weak));
extern "C" void DebugMon_Handler(void) {
  while(1)
    __NOP();
}

extern "C" void PVD_IRQHandler(void) __attribute__((weak));
extern "C" void PVD_IRQHandler(void) {
  while(1)
    __NOP();
}

extern "C" void RTC_IRQHandler(void) __attribute__((weak));
extern "C" void RTC_IRQHandler(void) {
  while(1)
  __NOP();
}

extern "C" void FLASH_IRQHandler(void) __attribute__((weak));
extern "C" void FLASH_IRQHandler(void) {
  while(1)
    __NOP();
}

extern "C" void RCC_IRQHandler(void) __attribute__((weak));
extern "C" void RCC_IRQHandler(void) {
  while(1)
    __NOP();
}

extern "C" void EXTI0_1_IRQHandler(void) __attribute__((weak));
void EXTI0_1_IRQHandler(void) {
  while(1)
    __NOP();
}

extern "C" void EXTI2_3_IRQHandler(void) __attribute__((weak));
extern "C" void EXTI2_3_IRQHandler(void) {
  while(1)
    __NOP();
}

extern "C" void EXTI4_15_IRQHandler(void) __attribute__((weak));
extern "C" void EXTI4_15_IRQHandler(void) {
  while(1)
    __NOP();
}

extern "C" void DMA1_Channel1_IRQHandler(void) __attribute__((weak));
extern "C" void DMA1_Channel1_IRQHandler(void) {
  while(1)
    __NOP();
}

extern "C" void DMA1_Channel2_3_IRQHandler(void) __attribute__((weak));
extern "C" void DMA1_Channel2_3_IRQHandler(void) {
  while(1)
    __NOP();
}

#ifndef STM32l011xx
extern "C" void DMA1_Channel4_5_6_7_IRQHandler(void) __attribute__((weak));
extern "C" void DMA1_Channel4_5_6_7_IRQHandler(void) {
  while(1)
    __NOP();
}
#endif

// This IRQ hander applies only to stm32l011xx. See startup/stm32l011xx_aranet.s
// for details.
#ifdef STM32l011xx
extern "C" void DMA1_Channel4_5_IRQHandler(void) __attribute__((weak));
extern "C" void DMA1_Channel4_5_IRQHandler(void) {
  while(1)
    __NOP();
}
#endif

extern "C" void ADC1_COMP_IRQHandler(void) __attribute__((weak));
extern "C" void ADC1_COMP_IRQHandler(void) {
  while(1)
    __NOP();
}

extern "C" void LPTIM1_IRQHandler(void) __attribute__((weak));
extern "C" void LPTIM1_IRQHandler(void) {
  SET_BIT(LPTIM1->ICR, LPTIM_ICR_ARRMCF);
  NVIC_ClearPendingIRQ(LPTIM1_IRQn);
  __NOP();
}

extern "C" void TIM2_IRQHandler(void) __attribute__((weak));
extern "C" void TIM2_IRQHandler(void) {
  while(1)
    __NOP();
}

extern "C" void TIM6_IRQHandler(void) __attribute__((weak));
extern "C" void TIM6_IRQHandler(void) {
  while(1)
    __NOP();
}

extern "C" void TIM21_IRQHandler(void) __attribute__((weak));
extern "C" void TIM21_IRQHandler(void) {
  while(1)
    __NOP();
}

extern "C" void TIM22_IRQHandler(void) __attribute__((weak));
extern "C" void TIM22_IRQHandler(void) {
  while(1)
    __NOP();
}

extern "C" void I2C1_IRQHandler(void) __attribute__((weak));
extern "C" void I2C1_IRQHandler(void) {
  while(1)
    __NOP();
}

extern "C" void I2C2_IRQHandler(void) __attribute__((weak));
extern "C" void I2C2_IRQHandler(void) {
  while(1)
    __NOP();
}

extern "C" void SPI1_IRQHandler(void) __attribute__((weak));
extern "C" void SPI1_IRQHandler(void) {
  while(1)
    __NOP();
}

extern "C" void SPI2_IRQHandler(void) __attribute__((weak));
extern "C" void SPI2_IRQHandler(void) {
  while(1)
    __NOP();
}

extern "C" void USART1_IRQHandler(void) __attribute__((weak));
extern "C" void USART1_IRQHandler(void) {
  while(1)
    __NOP();
}

extern "C" void USART2_IRQHandler(void) __attribute__((weak));
extern "C" void USART2_IRQHandler(void) {
  while(1)
    __NOP();
}

extern "C" void LPUART1_IRQHandler(void) __attribute__((weak));
extern "C" void LPUART1_IRQHandler(void) {
  while(1)
    __NOP();
}
