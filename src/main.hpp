#ifndef __MAIN_HPP
#define __MAIN_HPP

#include "stm32l0xx.h"

struct {
    GPIO_TypeDef* ledport;
    uint8_t lednum;
} leds_t;

leds_t leds[9];

#endif