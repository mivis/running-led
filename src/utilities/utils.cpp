#include "utils.hpp"

#include "stm32l0xx.h"

#include "board_config.hpp"

volatile uint32_t cUtils::ticks = 0;
void (*cUtils::delayMsCustom)(uint32_t) = 0;


void cUtils::delayMs(uint32_t ms) {
  if(ms == 0)
    return;

  if(delayMsCustom)  // But Timers operation can be so long, thus use sleepless delay version
    delayMsCustom(ms);
  else
    waitTicks(ms);
}

uint32_t cUtils::getTicks() {
  return ticks;
}

void cUtils::waitTicks(uint32_t tick) {
  uint32_t startTick = ticks;
  // if ticks overflows, compiler gives negative value, which it convert to unsigned by inverting and adding 1
  while((ticks - startTick) < tick)
    __NOP();
}

void cUtils::waitSWTicks(uint32_t tick) {
  uint32_t ticks = 0;
  while(ticks < tick){
    ticks++;
  }
}

void cUtils::enterSleepMode() {
  RCC->APB1ENR |= RCC_APB1ENR_PWREN;

  SCB->SCR &= ~SCB_SCR_SLEEPDEEP_Msk;  // SLEEPDEEP = 0

  PWR->CR |= PWR_CR_LPSDSR; // When bit set, the regulator switches in low-power mode when the CPU enters sleep

  //CLEAR_BIT(SysTick->CTRL, SysTick_CTRL_ENABLE_Msk);
  __WFI();
  //SET_BIT(SysTick->CTRL, SysTick_CTRL_ENABLE_Msk);
}

void cUtils::enterLPSleepMode() {
#ifdef DEBUG
	enterStopMode();
#else
  /* Enable Clocks */
  SET_BIT(RCC->APB1ENR, RCC_APB1ENR_PWREN);

  FLASH->ACR |= FLASH_ACR_SLEEP_PD; // switch off the Flash memory (optional) [reduces power consumption but increases the wake-up time]

  // Each digital IP clock must be enabled/disabled by using the RCC_APxxENR
  CLEAR_BIT(RCC->AHBENR, RCC_AHBENR_MIFEN); // This reset can be activated only when the NVM is in power-down mode.

  // System frequency should not exceed f_MSI range1 (001 - around 131.072 kHz)
  MODIFY_REG(RCC->ICSCR, RCC_ICSCR_MSIRANGE, RCC_ICSCR_MSIRANGE_0); // range0 <= range1, thus is acceptible

  // When set, regulator switches in low-power mode
  MODIFY_REG(PWR->CR, PWR_CR_LPSDSR, PWR_CR_LPSDSR);  // regulator goes back to Main mode when the CPU exits from LPsleep

  // This mode can only be entered when VCORE(VOS bits) is in range 2 (reset default)

  SET_BIT(PWR->CR, PWR_CR_ULP);    // When set, the VREFINT is switched off in low-power mode
  SET_BIT(PWR->CR, PWR_CR_FWU);    // FastWakeUp - VREFINT startup time is ignored when exiting from low-power mode

  CLEAR_BIT(RCC->APB1ENR, RCC_APB1ENR_PWREN);
  RCC->IOPSMENR = 0; // no need GPIO clock in sleep modes (TIM21/22 clocks through AF)

  CLEAR_BIT(SCB->SCR, SCB_SCR_SLEEPDEEP_Msk);  // clear bit SLEEPDEEP (0 - CPU use Sleep; 1 - CPU use Deep Sleep as its Low Power mode)

  CLEAR_BIT(SysTick->CTRL, SysTick_CTRL_ENABLE_Msk);
  __WFI();  // enter low-power mode
  SET_BIT(SysTick->CTRL, SysTick_CTRL_ENABLE_Msk);

  // Don't debug following 2 rows (after wfi (in run mode) some MCU GPR will be cleared, compiler doesn't suspect that)
  SET_BIT(RCC->APB1ENR, RCC_APB1ENR_PWREN);
  MODIFY_REG(RCC->ICSCR, RCC_ICSCR_MSIRANGE, RCC_ICSCR_MSIRANGE_6); // return back MSI 4,194MHz selected
#endif
}

void cUtils::enterStopMode() {
  /* Enable Clocks */
  RCC->APB1ENR |= RCC_APB1ENR_PWREN;

  /* Prepare to enter stop mode */
  SET_BIT(PWR->CR, PWR_CR_CWUF);    // clear CPU wake-up flag by writing 1 (after 2 clock cycles)
  CLEAR_BIT(PWR->CR, PWR_CR_PDDS);  // When CPU enters DeepSleep (after WFI), if bit set - perform CPU Power-down(Standby), else (Stop)

  // This could be only for stm32l051, should be checked
  MODIFY_REG(PWR->CR, PWR_CR_LPSDSR, PWR_CR_LPSDSR);  // When set, regulator switches in low-power mode (Only! for DeepSleep(StopMode & StandbyMode), Sleep or
                                                      // Low-powerRun modes)
  SET_BIT(PWR->CR, PWR_CR_ULP);    // When set, the VREFINT is switched off in low-power mode
  SET_BIT(PWR->CR, PWR_CR_FWU);    // FastWakeUp - VREFINT startup time is ignored when exiting from low-power mode

  CLEAR_BIT(RCC->CFGR, RCC_CFGR_STOPWUCK);   // Select MSI source after exiting Stop mode. Bit cleared = Internal 64 KHz to 4 MHz (MSI)
                                             // oscillator selected
  SET_BIT(SCB->SCR, SCB_SCR_SLEEPDEEP_Msk);  // Set bit SLEEPDEEP (0 - CPU use Sleep; 1 - CPU use Deep Sleep as its Low Power mode)

  SET_BIT(PWR->CR, PWR_CR_CWUF);    // clear CPU wake-up flag by writing 1 (after 2 clock cycles)
  CLEAR_BIT(SysTick->CTRL, SysTick_CTRL_ENABLE_Msk);
  __WFI();  // enter low-power mode
  SET_BIT(SysTick->CTRL, SysTick_CTRL_ENABLE_Msk);

#if defined EMULATOR || defined DEV_BOARD
  // Although not documented, HSI16 clock is not reenabled after exiting stop mode.
  // Even though it is explicity set to run and actually runs DURING stop mode.
  // Reconfigure the clock here as needed
  RCC->CR |= RCC_CR_HSION;
#endif
}

void cUtils::enterStandbyMode() {
  /* Enable Clocks */
  RCC->APB1ENR |= RCC_APB1ENR_PWREN;

  /* Prepare to enter stop mode */
  SET_BIT(PWR->CR, PWR_CR_CWUF);  // clear CPU wake-up flag by writing 1 (after 2 clock cycles)
  SET_BIT(PWR->CR, PWR_CR_PDDS);  // When CPU enters DeepSleep (after WFI), if bit set - perform CPU Power-down(Standby), else (Stop)

  // This could be only for stm32l051, should be checked
  MODIFY_REG(PWR->CR, PWR_CR_LPSDSR, PWR_CR_LPSDSR);  // When set, regulator switches in low-power mode (Only! for DeepSleep(StopMode & StandbyMode), Sleep or
                                                      // Low-powerRun modes)

  SET_BIT(SCB->SCR, SCB_SCR_SLEEPDEEP_Msk);    // Set bit SLEEPDEEP (0 - CPU use Sleep; 1 - CPU use Deep Sleep as its Low Power mode)
  SET_BIT(SCB->SCR, SCB_SCR_SLEEPONEXIT_Msk);  // reenter low-power mode after ISR

  CLEAR_BIT(SysTick->CTRL, SysTick_CTRL_ENABLE_Msk);
  __WFI();  // enter low-power mode
  SET_BIT(SysTick->CTRL, SysTick_CTRL_ENABLE_Msk);
}

uint32_t cUtils::getChipUID() {
  return *((uint32_t*)(UID_BASE + 0x14));
}
