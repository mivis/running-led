#include "utilities/general.hpp"
#include "interfaces/gpio.hpp"

#include "stm32l0xx.h"


void cGENERAL::mcuDebugConfig() {
  SET_BIT(RCC->APB2ENR, RCC_APB2ENR_DBGMCUEN);
  for(uint8_t i = 0; i < 10; i++) {
    __NOP();
  }
#ifdef DEBUG
  // in Debug mode:
  // - RTC(TXinterval) not running (The clock of the RTC counter is stopped when the core is halted)
  // - LPTIM(delayMS()) is running (LPTIM1 counter clock is fed even if the core is halted)
  __BKPT();
  SET_BIT(DBGMCU->CR, DBGMCU_CR_DBG_SLEEP | DBGMCU_CR_DBG_STOP | DBGMCU_CR_DBG_STANDBY);
  SET_BIT(DBGMCU->APB1FZ,
          DBGMCU_APB1_FZ_DBG_WWDG_STOP | DBGMCU_APB1_FZ_DBG_IWDG_STOP | DBGMCU_APB1_FZ_DBG_RTC_STOP | DBGMCU_APB1_FZ_DBG_I2C1_STOP);
#else
  CLEAR_BIT(DBGMCU->CR, DBGMCU_CR_DBG_SLEEP | DBGMCU_CR_DBG_STOP | DBGMCU_CR_DBG_STANDBY);
  CLEAR_BIT(DBGMCU->APB1FZ,
            DBGMCU_APB1_FZ_DBG_WWDG_STOP | DBGMCU_APB1_FZ_DBG_IWDG_STOP | DBGMCU_APB1_FZ_DBG_RTC_STOP | DBGMCU_APB1_FZ_DBG_I2C1_STOP);

  // Disable SWDIO & SWCLK
  SET_BIT(RCC->IOPENR, RCC_IOPENR_GPIOAEN);

  for(uint8_t i = 0; i < 10; i++) {
    __NOP();
  }

  cGPIO::pinConf(SWD_PORT, SWD_PIN, cGPIO::ANALOG); // DIGITAL_INPUT
  cGPIO::pinConf(SWD_PORT, SWD_PIN, cGPIO::PUPD_PD);

  cGPIO::pinConf(SWC_PORT, SWC_PIN, cGPIO::ANALOG);
  cGPIO::pinConf(SWC_PORT, SWC_PIN, cGPIO::PUPD_PD);
#endif

  // Disable the enabled clocks
  CLEAR_BIT(RCC->APB2ENR, RCC_APB2ENR_DBGMCUEN);
  CLEAR_BIT(RCC->IOPENR, RCC_IOPENR_GPIOAEN);
}


