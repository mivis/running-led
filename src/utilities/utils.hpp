#ifndef __UTILS_HPP
#define __UTILS_HPP

#include "board_config.hpp"
#include <cstdint>


// addon to CMSIS Cortex-M0+ Device Peripheral Access Layer Header File
#define TOGGLE_BIT(REG, BIT) ((REG) ^= (BIT))

class cUtils {
  volatile static uint32_t ticks;
  static void (*delayMsCustom)(uint32_t);

 public:

  static void tick() { ticks++; };
  static uint32_t getTicks();
  static void waitTicks(uint32_t tick);
  static void waitSWTicks(uint32_t tick);
  static void delayMs(uint32_t ms);
  static void setCustomDelayMs(void (*customDelay)(uint32_t)) { delayMsCustom = customDelay; };
  static void enterSleepMode();
  static void enterLPSleepMode();
  static void enterStopMode();
  static void enterStandbyMode();
  static uint32_t getChipUID();
};

#endif  // __UTILS_HPP
