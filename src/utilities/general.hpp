#ifndef _GENERAL_HPP
#define _GENERAL_HPP

#include <cstdint>

#include "utilities/utils.hpp"
#include "board_config.hpp"

class cGENERAL {
 public:

  static void mcuDebugConfig();
  static uint16_t generateDelayMs();
  static uint16_t generateDelayMs(uint16_t from, uint16_t to);
};

#endif
