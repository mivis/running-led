#include <cstdint>

#include "stm32l0xx.h"

#include "interfaces/led.hpp"
#include "interfaces/gpio.hpp"

#include "peripherals/lptimer.hpp"

#include "utilities/general.hpp"
#include "utilities/utils.hpp"

// usually LED's placed in schematic unevenly, so used such approach
cLED leds[] = {cLED(GPIOA,15), cLED(GPIOA,12), cLED(GPIOA,11), cLED(GPIOA,10), cLED(GPIOA,9),
               cLED(GPIOB,2), cLED(GPIOA,3), cLED(GPIOA,1), cLED(GPIOB,8)};

uint8_t direction = 1;

void initialize() {
  MODIFY_REG(RCC->ICSCR, RCC_ICSCR_MSIRANGE, 0b110 << RCC_ICSCR_MSIRANGE_Pos); // MSI 4,194MHz selected

  cGENERAL::mcuDebugConfig();
  SysTick_Config(MSI_freq / 1000);

  cLPTimer::init();
  cUtils::setCustomDelayMs(cLPTimer::delayMs);

  cGPIO::pinConf(BUTTON_PORT, BUTTON_PIN, cGPIO::DIGITAL_INPUT);
  cGPIO::pinConf(BUTTON_PORT, BUTTON_PIN, cGPIO::PUPD_PU);
  RCC->APB2ENR |= RCC_APB2ENR_SYSCFGEN; // enable clock for SYSCFG module
  SYSCFG->EXTICR[1] |= SYSCFG_EXTICR2_EXTI4_PB; // pin 4 port B
  EXTI->IMR |= EXTI_IMR_IM4;     // Activate interrupt (external line 4 Connected to the GPIO)
  EXTI->FTSR |= EXTI_FTSR_TR4;   // Enable falling Trigger
  NVIC_EnableIRQ(EXTI4_15_IRQn);  // Lines grouped at three groups: EXTI0_1_IRQn; EXTI2_3_IRQn; EXTI4_15_IRQn
  NVIC_SetPriority(EXTI4_15_IRQn, 0);
}


int main(void) {

	initialize();
	
	while(1) {
    if(direction){
      for(int8_t i=0; i<9; i++){
        leds[i].set(true);
        cUtils::delayMs(500);
        leds[i].set(false);
      }
    }else {
      for(int8_t i=8; i>=0; i--){
        leds[i].set(true);
        cUtils::delayMs(500);
        leds[i].set(false);
      }
    }

  }; // END while(1){

    return 0;
}; // END int main(void)


extern "C" void EXTI4_15_IRQHandler(void){
  cUtils::waitSWTicks(69900); // 100ms

  if(!(BUTTON_PORT->IDR & (1 << BUTTON_PIN))) { // if Button is pressed
    direction ^= 1;
    }

  EXTI->PR |= EXTI_PR_PR4; // Clear Pending Interrupt Flag by writing 1
}