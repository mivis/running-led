#ifndef __RCC_HPP
#define __RCC_HPP

#include "stm32l0xx.h"

class cRCC {
 public:
  static void enableLSE();
  static void enableLSI();
  static void disableLSI();
};


//------ Control Register -------------------------------------------

//------ IOPEN Register -------------------------------------------
#define RCC_IOPENR_IOPA_OFF (uint32_t)0b0  //
#define RCC_IOPENR_IOPA_ON  (uint32_t)0b1  //

#define RCC_IOPENR_IOPB_OFF (uint32_t)0b0  //
#define RCC_IOPENR_IOPB_ON  (uint32_t)0b1  //

#define RCC_IOPENR_IOPC_OFF (uint32_t)0b0  //
#define RCC_IOPENR_IOPC_ON  (uint32_t)0b1  //

#define RCC_IOPENR_IOPD_OFF (uint32_t)0b0  //
#define RCC_IOPENR_IOPD_ON  (uint32_t)0b1  //

#define RCC_IOPENR_IOPE_OFF (uint32_t)0b0  //
#define RCC_IOPENR_IOPE_ON  (uint32_t)0b1  //

#define RCC_IOPENR_IOPH_OFF (uint32_t)0b0  //
#define RCC_IOPENR_IOPH_ON  (uint32_t)0b1  //


//------ ICSC Register -------------------------------------------

#ifdef STM32L011xx
  #define RCC_ICSCR_MSIRANGE_65K  0b000 << RCC_ICSCR_MSIRANGE_Pos  // around 65.536 kHz
  #define RCC_ICSCR_MSIRANGE_131K 0b001 << RCC_ICSCR_MSIRANGE_Pos  // around 131.072 kHz
  #define RCC_ICSCR_MSIRANGE_262K 0b010 << RCC_ICSCR_MSIRANGE_Pos  // around 262.144 kHz
  #define RCC_ICSCR_MSIRANGE_524K 0b011 << RCC_ICSCR_MSIRANGE_Pos  // around 524.288 kHz
  #define RCC_ICSCR_MSIRANGE_1M   0b100 << RCC_ICSCR_MSIRANGE_Pos  // around 1.048 MHz
  #define RCC_ICSCR_MSIRANGE_2M   0b101 << RCC_ICSCR_MSIRANGE_Pos  // around 2.097 MHz (reset value)
  #define RCC_ICSCR_MSIRANGE_4M   0b110 << RCC_ICSCR_MSIRANGE_Pos  // around 4.194 MHz
#endif
#ifdef STM32L051xx
  #define RCC_ICSCR_MSIRANGE_65K  RCC_ICSCR_MSIRANGE_0  // around 65.536 kHz
  #define RCC_ICSCR_MSIRANGE_131K RCC_ICSCR_MSIRANGE_1  // around 131.072 kHz
  #define RCC_ICSCR_MSIRANGE_262K RCC_ICSCR_MSIRANGE_2  // around 262.144 kHz
  #define RCC_ICSCR_MSIRANGE_524K RCC_ICSCR_MSIRANGE_3  // around 524.288 kHz
  #define RCC_ICSCR_MSIRANGE_1M   RCC_ICSCR_MSIRANGE_4  // around 1.048 MHz
  #define RCC_ICSCR_MSIRANGE_2M   RCC_ICSCR_MSIRANGE_5  // around 2.097 MHz (reset value)
  #define RCC_ICSCR_MSIRANGE_4M   RCC_ICSCR_MSIRANGE_6  // around 4.194 MHz
#endif

int RCC_Init(void);

#endif  // __RCC_HPP
