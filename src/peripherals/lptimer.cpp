#include "lptimer.hpp"

#include "stm32l0xx.h"

#include "src/board_config.hpp"
#include "peripherals/rcc.hpp"
#include "utilities/utils.hpp"
#include "utilities/general.hpp"

bool cLPTimer::wakeupOccur = false;

void cLPTimer::init() {
  SET_BIT(RCC->APB1ENR, RCC_APB1ENR_LPTIM1EN);
  SET_BIT(RCC->APB1SMENR, RCC_APB1SMENR_LPTIM1SMEN);

  cRCC::enableLSE();
  MODIFY_REG(RCC->CCIPR, RCC_CCIPR_LPTIM1SEL,
             (RCC_CCIPR_LPTIM1SEL_1 | RCC_CCIPR_LPTIM1SEL_0));  // Use LSE
                                                                // (32kHz)
  SET_BIT(LPTIM1->CFGR,
          LPTIM_CFGR_PRESC_0 | LPTIM_CFGR_PRESC_2);  // Internal clock, /32
                                                     // prescale 32k -> 1k
  SET_BIT(LPTIM1->IER, LPTIM_IER_ARRMIE);
  SET_BIT(LPTIM1->CR, LPTIM_CR_ENABLE);

  SET_BIT(EXTI->IMR, EXTI_IMR_IM29);  // Activate LPTimer Wakeup

  NVIC_SetPriority(LPTIM1_IRQn, 0);
  NVIC_EnableIRQ(LPTIM1_IRQn);
}

void cLPTimer::deinit() {
  CLEAR_BIT(LPTIM1->CR, LPTIM_CR_ENABLE);
  CLEAR_BIT(RCC->APB1ENR, RCC_APB1ENR_LPTIM1EN);
  CLEAR_BIT(RCC->APB1SMENR, RCC_APB1SMENR_LPTIM1SMEN);

  CLEAR_BIT(EXTI->IMR, EXTI_IMR_IM29);  // Deactivate LPTimer Wakeup

  NVIC_DisableIRQ(LPTIM1_IRQn);
}

// for delays < 5ms error can be up to minus 1ms (cause systick counter always running and
// getTicks() can be called just before next tick will occur), thus
// [never use delays < 2ms],
// use DELAY_US() instead
void cLPTimer::delayMs(uint32_t delayLength) {
  if(delayLength <= 1) {
    delayLength = 2;
  }

  if(delayLength < 5)                // delays < 5ms not use sleep, cause that add significant error at such short delays
    cUtils::waitTicks(delayLength);  // based on SysTick timer, resetting each time not used (to support nested delays)
  else {
    delayLength -= 2;
    LPTIM1->ARR = delayLength;
    LPTIM1->CR |= LPTIM_CR_SNGSTRT;

	cUtils::enterStopMode();
  }
}


uint16_t cLPTimer::getTicks() {
  return (uint16_t)LPTIM1->CNT;
}
