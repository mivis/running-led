/*
 * EXTI.h
 *
 *  Created on: 2018. gada 4. j�n.
 *      Author: mihailsz
 */

#ifndef EXTI_H_
#define EXTI_H_

#define EXTI_IMR_IM0_M  0  // Interrupt     Masked (NOT active)
#define EXTI_IMR_IM3_M  0  // Interrupt     Masked (NOT active)
#define EXTI_IMR_IM20_M 0  // Interrupt     Masked (NOT active)
#define EXTI_IMR_IM21_M 0  // Interrupt     Masked (NOT active)
#define EXTI_IMR_IM29_M 0  // Interrupt     Masked (NOT active)

#define EXTI_IMR_IM0_NM  1  // Interrupt not Masked (active)
#define EXTI_IMR_IM3_NM  1  // Interrupt not Masked (active)
#define EXTI_IMR_IM20_NM 1  // Interrupt not Masked (active)
#define EXTI_IMR_IM21_NM 1  // Interrupt not Masked (active)
#define EXTI_IMR_IM29_NM 1  // Interrupt not Masked (active)

#define EXTI_RTSR_RT0_DIS  0  // Rising trigger Disabled
#define EXTI_RTSR_RT20_DIS 0  // Rising trigger Disabled
#define EXTI_RTSR_RT21_DIS 0  // Rising trigger Disabled
#define EXTI_RTSR_RT29_DIS 0  // Rising trigger Disabled

#define EXTI_RTSR_RT0_EN  1  // Rising trigger Enabled
#define EXTI_RTSR_RT20_EN 1  // Rising trigger Enabled
#define EXTI_RTSR_RT21_EN 1  // Rising trigger Enabled
#define EXTI_RTSR_RT29_EN 1  // Rising trigger Enabled

#define EXTI_FTSR_FT3_DIS 0  // Falling trigger Disabled

#define EXTI_FTSR_FT3_EN 1  // Falling trigger Enabled


#endif /* EXTI_H_ */
