#ifndef __LPTIMER_HPP
#define __LPTIMER_HPP

#include <cstdint>

class cLPTimer {
 public:
  static bool wakeupOccur;
  static void init();
  static void deinit();
  static void delayMs(uint32_t delayLength);
  static void delayMsSleep(uint32_t delayLength);
  static uint16_t getTicks();
};

#endif  // __LPTIMER_HPP
