#include "rcc.hpp"

#include "stm32l0xx.h"
#include "src/board_config.hpp"

#include "utilities/utils.hpp"

void cRCC::enableLSE() {
  if(READ_BIT(RCC->CSR, RCC_CSR_LSERDY))
    return;

  // The LSEON,LSEBYP,RTCSEL,LSEDRV,RTCEN bits in the RCC_CSR are in the RTC domain.
  // These bits are write protected after reset,the DBP bit in the PWR_CR set to modify them.
  SET_BIT(RCC->APB1ENR, RCC_APB1ENR_PWREN);// power interface not need to be enabled if LSE already running
  SET_BIT(PWR->CR, PWR_CR_DBP);

  // Cl change for 0.5pF will change Freq. for 0,203554816 Hz
  #if defined(VOLTAGE) || defined(CONTACT)
    MODIFY_REG(RCC->CSR, RCC_CSR_LSEDRV, RCC_CSR_LSEDRV_1);// RCC_CSR_LSEDRV_x (not return to default until restarting Power)
  #else
    MODIFY_REG(RCC->CSR, RCC_CSR_LSEDRV, 0);
  #endif

  while(!READ_BIT(RCC->CSR, RCC_CSR_LSEON))// This bit is set and cleared by software, no special need to loop
	  SET_BIT(RCC->CSR, RCC_CSR_LSEON);  // Low-drive used by default

  uint16_t ms = 0;
  uint8_t Gm = (RCC->CSR & RCC_CSR_LSEDRV) >> 11; // Gm_crit_max (Oscillator transconductance Gm = Gm_crit_max x 5 ("oscillator gain"))
  while(!READ_BIT(RCC->CSR, RCC_CSR_LSERDY)){ // anyway must go out from loop with succesful launch or not go out at all.
	if(ms++ > 5000){ // if 5s not enough to launch quartz, default Gain is dangerous
		ms = 0;
		Gm++;
		MODIFY_REG(RCC->CSR, RCC_CSR_LSEDRV, Gm << 11); // change on-the-fly (after enabling it) supported
	}
    cUtils::delayMs(1);
  }

}

void cRCC::enableLSI() {
  // The LSEON, LSEBYP, RTCSEL,LSEDRV and RTCEN bits in the RCC control and status
  // register (RCC_CSR) are in the RTC domain. As these bits are write protected after reset,
  // the DBP bit in the Power control register (PWR_CR) has to be set to be able to modify them.
  SET_BIT(RCC->APB1ENR, RCC_APB1ENR_PWREN);
  SET_BIT(PWR->CR, PWR_CR_DBP);

  // 0.5pF will change Freq. for 0,203554816 Hz
  // Freq. measured 32768,047972352Hz (from 0.500000732Hz) with 22pF capacitors
  // TODO: driving ability (pullability) test with 140kOhm to achieve safety factor #3 [Medium-high drive selected]

  while(!READ_BIT(RCC->CSR, RCC_CSR_LSION))
    SET_BIT(RCC->CSR, RCC_CSR_LSION);  // Low-drive used by default

  while(!READ_BIT(RCC->CSR, RCC_CSR_LSIRDY))
    __NOP();
}

void cRCC::disableLSI() {
  CLEAR_BIT(RCC->CSR, RCC_CSR_LSION);
}
